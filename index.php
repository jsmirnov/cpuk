<?php

ini_set('date.timezone', 'UTC');

// for the sake of simplicity decided not to use autoloader.
require_once "./src/model/Item.php";
require_once "./src/model/Vendor.php";
require_once "./src/model/Lookup.php";
require_once "./src/model/Response.php";
require_once "./src/model/Request.php";

require_once "./src/service/FileLookup.php";
require_once "./src/service/RequestValidator.php";
require_once "./src/service/RequestParser.php";

require_once "./src/Application.php";
require_once "./src/Bootstrap.php";


$longOptions = array(
    "filename:",
    "day:",
    "time:",
    "location:",
    "covers:"
);

$options = getopt("", $longOptions);

$request = Request::build(getcwd() . '/' . $options['filename'], $options['day'], $options['time'], $options['location'], $options['covers'], new DateTime('now'));

/**
 * @var Response $response
 */
$response = Bootstrap::getApplication()->process($request);

if ($response->getError()) {
    echo $response->getError()->getMessage() . PHP_EOL;
    exit(1);
}

/**
 * @var Vendor[] $vendors
 */
$vendors = $response->getVendors();

if (count($vendors) === 0) {
    echo "Nothing found" . PHP_EOL;
    exit(0);
}

foreach ($vendors as $vendor) {
    /**
     * @var Vendor $vendor
     */
    // there was no requirement for vendor name to be in output
    foreach ($vendor->getItems() as $item) {
        /**
         * @var Item $item
         */
        // one note here - in example output 2 rows are different. one is with ;; at then end, another is not. I assume that ; is only a delimiter.
        // Anyway should not be the most important thing - as this is only output formatting.
        echo join(";", [$item->getName(), $item->getAllergies()]) . PHP_EOL;
    }
}