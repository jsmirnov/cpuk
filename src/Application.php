<?php

class Application
{

    /**
     * @var RequestParser
     */
    protected $parser;

    /**
     * @var FileLookup
     */
    protected $fileLookup;

    /**
     * Application constructor.
     * @param RequestParser $parser
     * @param FileLookup $fileLookup
     */
    public function __construct($parser, $fileLookup)
    {
        $this->parser = $parser;
        $this->fileLookup = $fileLookup;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function process($request)
    {
        $result = null;
        try {
            $lookup = $this->parser->parseRequest($request);
            $vendors = $this->fileLookup->lookup($lookup);
            $result = new Response($vendors);
        } catch (Exception $exception) {
            $result = new Response(null, $exception);
        }

        return $result;
    }
}