<?php

class RequestParser
{
    /**
     * @var RequestValidator
     */
    protected $validator;

    /**
     * RequestParser constructor.
     * @param RequestValidator $validator
     */
    public function __construct($validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @return Lookup
     * @throws Exception
     */
    public function parseRequest($request)
    {
        if (!$this->validator->isFileExists($request)) {
            throw new Exception('File does not exist.');
        }

        $deliveryTime = DateTime::createFromFormat('d/m/y H:i', $request->getDay() . ' ' . $request->getTime());

        if (!$deliveryTime) {
            throw new Exception('Date time is not properly structured');
        }

        if (!$this->validator->isDeliveryInFuture($deliveryTime)) {
            throw new Exception('Delivery time should be in future.');
        }

        return new Lookup($request->getFilename(), $deliveryTime, $request->getSearchTime(), $request->getLocation(), $request->getCovers());
    }
}