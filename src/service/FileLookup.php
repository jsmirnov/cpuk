<?php

class FileLookup
{

    /**
     * @param Lookup $lookup
     * @return Vendor[]
     * @throws Exception
     */
    public function lookup($lookup)
    {
        $handle = fopen($lookup->getFilename(), "r");
        if (!$handle) {
            throw new Exception('Can not open file for read');
        }

        $vendors = array();
        $currentVendor = null;
        $isVendorDeliveringToArea = false;

        while (($line = fgets($handle)) !== false) {
            $isNewLine = $this->isNewLine($line);

            if ($isNewLine) {
                $currentVendor = null;
                continue;
            }

            $vendor = $this->getVendor($line);

            if ($vendor && $currentVendor) {
                throw new Exception('File is malformed. No new line between vendors.');
            }

            if ($vendor) {
                $currentVendor = $vendor;
                $isVendorDeliveringToArea = $vendor->isDeliveringToArea($lookup->getLocation());
                continue;
            }

            if (!$isVendorDeliveringToArea) {
                continue;
            }

            $item = $this->getItem($line);

            if (!$item) {
                throw new Exception('File is malformed. Row in unknown format.');
            }

            if (!$currentVendor && $item) {
                throw new Exception('File is malformed. Item is not under vendor.');
            }

            if ($item->getAdvanceTime() > $lookup->getNoticeHours()) {
                continue;
            }

            $currentVendor->addItem($item);

            $vendors[$currentVendor->getName()] = $currentVendor;
        }

        fclose($handle);

        return array_values($vendors);
    }

    /**
     * @param string $line
     * @return Vendor|null
     */
    protected function getVendor($line)
    {
        $vendorRegexp = "/^([A-Za-z ]*);([A-Za-z][A-Za-z0-9]*);(\d*)$/";
        $isMatch = preg_match_all($vendorRegexp, $line, $matches);

        if (!$isMatch) {
            return null;
        }

        return new Vendor($matches[1][0], $matches[2][0], $matches[3][0]);
    }

    /**
     * @param string $line
     * @return Item|null
     */
    protected function getItem($line)
    {
        // pattern for allergies missing
        $itemRegexp = "/^([A-Za-z ]*);([\w,]*);(\d\dh)$/";
        $isMatch = preg_match_all($itemRegexp, $line, $matches);

        if (!$isMatch) {
            return null;
        }

        return new Item($matches[1][0], $matches[2][0], floatval(str_replace('h', '', $matches[3][0])));
    }

    protected function isNewLine($line)
    {
        return preg_match( '/^[\r\n]{0,2}$/', $line);
    }
}