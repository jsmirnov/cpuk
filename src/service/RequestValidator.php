<?php
// separate validator just for the sake of extra service and DI showcase.
// could be completely static as stateless service.
class RequestValidator {
    /**
     * @param Request $request
     * @return bool
     */
    public function isFileExists($request)
    {
        return file_exists($request->getFilename());
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isDeliveryInFuture($date)
    {
        return $date->getTimestamp() > time();
    }
}