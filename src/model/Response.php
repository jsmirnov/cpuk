<?php

class Response
{
    /**
     * @var Vendor[]
     */
    protected $vendors;
    /**
     * @var Error
     */
    protected $error;

    // Error might be moved out of constructor to set method. Though I'd prefer to have it immutable.

    /**
     * Response constructor.
     * @param Vendor[] $vendors
     * @param Error $error
     */
    public function __construct($vendors, $error = null)
    {
        $this->error = $error;
        $this->vendors = $vendors;
    }

    /**
     * @return Vendor[]
     */
    public function getVendors()
    {
        return $this->vendors;
    }

    /**
     * @return Error
     */
    public function getError()
    {
        return $this->error;
    }
}