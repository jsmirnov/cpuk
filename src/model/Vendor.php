<?php

class Vendor
{
    protected $name;
    protected $postcode;
    protected $areacode;
    protected $maxCovers;
    /**
     * @var Item[]
     */
    protected $items;

    /**
     * Vendor constructor.
     * @param string $name
     * @param string $postcode
     * @param number $maxCovers
     */
    public function __construct($name, $postcode, $maxCovers)
    {
        $this->name = $name;
        $this->postcode = $postcode;
        $this->areacode = strtolower(substr($postcode, 0, 2));
        $this->maxCovers = $maxCovers;
        $this->items = [];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @return number
     */
    public function getMaxCovers()
    {
        return $this->maxCovers;
    }

    /**
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param Item $item
     */
    public function addItem($item)
    {
        $this->items[] = $item;
    }

    /**
     * @param string $location
     * @return bool
     */
    public function isDeliveringToArea($location)
    {
        // assumption is that there are always 2 symbols to compare
        return $this->areacode === strtolower(substr($location, 0, 2));
    }
}