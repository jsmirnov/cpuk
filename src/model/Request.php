<?php

// this is an object for outside of the world to use and provide an input
class Request
{
    protected $filename;
    protected $day;
    protected $time;
    protected $location;
    protected $covers;
    protected $searchTime;

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @return mixed
     */
    public function getCovers()
    {
        return $this->covers;
    }

    /**
     * @return mixed
     */
    public function getSearchTime()
    {
        return $this->searchTime;
    }

    public static function build($filename, $day, $time, $location, $covers, $searchTime)
    {
        $request = new Request();

        $request->filename = $filename;
        $request->day = $day;
        $request->time = $time;
        $request->location = $location;
        $request->covers = $covers;
        $request->searchTime = $searchTime;

        return $request;
    }
}