<?php

/**
 * Class Item
 * This is a "matching" item, which will be populated under vendor only in case it is possible to deliver.
 */
class Item
{
    protected $name;
    protected $allergies;
    // "Item notice period must be less or equal to the difference between the search time and the actual time of the delivery"
    protected $advanceTime;

    /**
     * Item constructor.
     * @param string $name
     * @param string $allergies
     * @param int $advanceTime
     */
    public function __construct($name, $allergies, $advanceTime)
    {
        $this->name = $name;
        $this->allergies = $allergies;
        $this->advanceTime = $advanceTime;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAllergies()
    {
        return $this->allergies;
    }

    /**
     * @return int
     */
    public function getAdvanceTime()
    {
        return $this->advanceTime;
    }
}