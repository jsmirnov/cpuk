<?php

// this is an object for inside application use to move data around
class Lookup
{
    /**
     * @var string
     */
    protected $filename;

    /**
     * @var DateTime
     */
    protected $deliveryTime;

    /**
     * @var DateTime
     */
    protected $searchTime;

    /**
     * @var string
     */
    protected $location;

    /**
     * @var number
     */
    protected $covers;

    /**
     * @var int
     */
    protected $noticeHours;

    /**
     * Lookup constructor.
     * @param string $filename
     * @param DateTime $deliveryTime
     * @param DateTime $searchTime
     * @param string $location
     * @param number $covers
     */
    public function __construct($filename, DateTime $deliveryTime, DateTime $searchTime, $location, $covers)
    {
        $this->filename = $filename;
        $this->deliveryTime = $deliveryTime;
        $this->searchTime = $searchTime;
        $this->noticeHours = floatval(($deliveryTime->getTimestamp() - $searchTime->getTimestamp()) / 3600);
        $this->location = $location;
        $this->covers = $covers;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @return DateTime
     */
    public function getDeliveryTime()
    {
        return $this->deliveryTime;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @return number
     */
    public function getCovers()
    {
        return $this->covers;
    }

    /**
     * @return DateTime
     */
    public function getSearchTime()
    {
        return $this->searchTime;
    }

    /**
     * @return int
     */
    public function getNoticeHours()
    {
        return $this->noticeHours;
    }
}