<?php
// Application factory
class Bootstrap {
    public static function getApplication()
    {
        return new Application(Bootstrap::getRequestParser(), Bootstrap::getFileLookup());
    }

    /**
     * @return RequestParser
     */
    protected static function getRequestParser()
    {
        return new RequestParser(Bootstrap::getRequestValidator());
    }

    /**
     * @return RequestValidator
     */
    protected static function getRequestValidator()
    {
        return new RequestValidator();
    }

    /**
     * @return FileLookup
     */
    protected static function getFileLookup()
    {
        return new FileLookup();
    }
}