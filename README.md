# cpuk

* PHP 5.6
* No autoloader implemented.
* In some places used arrays instead of typed collections.
* Vendor and items under vendor are populated only if there is an option to deliver.

example run(from project root):
```
docker run --rm -it -v $(pwd):/app -w /app php:5.6.39-cli-alpine3.8 php index.php --filename example-input --day 31/01/19 --time 10:00 --location NW42QA --covers 10
Premium meat selection;
Breakfast;gluten,eggs
```

example test run(from project root):
```
cd test
docker run --rm -it -v $(pwd)/..:/app -w /app/test php:5.6.39-cli-alpine3.8 php index.php
```
