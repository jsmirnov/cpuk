<?php
ini_set('date.timezone', 'UTC');
ini_set('zend.assertions', 1);
ini_set('assert.exception', 1);
// Just for the sake of not using external tooling I'v used only assertions.
// Overall PHPUnit would provide better organization(suites and cases), better matching tooling and some other things.
// But the cases won't change much.
// As far as I remember there is a way to mock file system by using in memory handler, but I'v decided to use simple files.

require_once "../src/model/Item.php";
require_once "../src/model/Vendor.php";
require_once "../src/model/Lookup.php";
require_once "../src/model/Response.php";
require_once "../src/model/Request.php";

require_once "../src/service/FileLookup.php";
require_once "../src/service/RequestValidator.php";
require_once "../src/service/RequestParser.php";

require_once "../src/Application.php";
require_once "../src/Bootstrap.php";

$application = Bootstrap::getApplication();


function runApp($fileName, $day, $time, $location, $covers, $searchTime)
{
    $request = Request::build(getcwd() . '/' . $fileName, $day, $time, $location, $covers, $searchTime);
    return Bootstrap::getApplication()->process($request);
}


function testPositive()
{
    $resultPositive = runApp('files/test-file-positive', '01/02/19', '10:00', 'E32NY', 5, date_create_from_format('d/m/y h:i', '01/01/19 10:00'));
    assert(count($resultPositive->getVendors()) == 1);
    $vendor = $resultPositive->getVendors()[0];
    assert($vendor->getName() === 'Grain and Leaf');
    assert(count($vendor->getItems()) === 1);
    $item = $vendor->getItems()[0];
    assert($item->getName() === 'Grain salad');
}

function testNegative()
{
    $resultNegative = runApp('files/test-file-negative', '01/02/19', '10:00', 'E32NY', 5, date_create_from_format('d/m/y h:i', '01/01/19 10:00'));
    assert(count($resultNegative->getVendors()) == 0);
    assert($resultNegative->getError()->getMessage() === 'File is malformed. No new line between vendors.');
}

testPositive();
testNegative();
exit(0);